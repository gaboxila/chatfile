package Servidor;

import javax.swing.JFrame;

/**
 *
 * @author TOLEDO
 */
public class FrmServer extends javax.swing.JFrame {

    /**
     * Creates new form FrmChat
     */
    public FrmServer() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSpinner1 = new javax.swing.JSpinner();
        dsktpPane = new javax.swing.JDesktopPane();
        MenuBar = new javax.swing.JMenuBar();
        Conversion = new javax.swing.JMenu();
        miServer = new javax.swing.JMenuItem();
        Separator = new javax.swing.JPopupMenu.Separator();
        miSalir = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);

        javax.swing.GroupLayout dsktpPaneLayout = new javax.swing.GroupLayout(dsktpPane);
        dsktpPane.setLayout(dsktpPaneLayout);
        dsktpPaneLayout.setHorizontalGroup(
            dsktpPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 574, Short.MAX_VALUE)
        );
        dsktpPaneLayout.setVerticalGroup(
            dsktpPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 363, Short.MAX_VALUE)
        );

        getContentPane().add(dsktpPane, java.awt.BorderLayout.CENTER);

        Conversion.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Conversion.setText("Archivo");

        miServer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        miServer.setText("Servidor");
        miServer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miServerActionPerformed(evt);
            }
        });
        Conversion.add(miServer);
        Conversion.add(Separator);

        miSalir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_W, java.awt.event.InputEvent.CTRL_MASK));
        miSalir.setText("Salir");
        miSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSalirActionPerformed(evt);
            }
        });
        Conversion.add(miSalir);

        MenuBar.add(Conversion);

        setJMenuBar(MenuBar);

        setBounds(0, 0, 590, 427);
    }// </editor-fold>//GEN-END:initComponents

    private void miSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSalirActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_miSalirActionPerformed

    private void miServerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miServerActionPerformed
        // TODO add your handling code here:
        InternalServer is = new InternalServer();
        dsktpPane.add(is);
        is.setVisible(true);
    }//GEN-LAST:event_miServerActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmServer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmServer().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu Conversion;
    private javax.swing.JMenuBar MenuBar;
    private javax.swing.JPopupMenu.Separator Separator;
    private javax.swing.JDesktopPane dsktpPane;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JMenuItem miSalir;
    private javax.swing.JMenuItem miServer;
    // End of variables declaration//GEN-END:variables
}
