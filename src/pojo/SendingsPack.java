package pojo;

import java.io.Serializable;

/**
 *
 * @author TOLEDO
 */
public class SendingsPack implements Serializable{
    
    private String apodo, IP, mensaje;

    public String getApodo() {
        return apodo;
    }

    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
